import { Component, OnInit }        from '@angular/core';
import { style, state, animate, transition, trigger } from '@angular/core';
import { Router, NavigationExtras, ActivatedRoute, NavigationEnd } from '@angular/router';
import { AuthService }      from '~services/auth.service';
import { AuthGuard }      from '~services/auth-guard.service';
import { Observable }       from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/filter';

import { MeUser } from '~models/user.model';

@Component({
    templateUrl: '../views/login.view.html',
    animations: [
        trigger('fadeLogin', [
            transition(':enter', [   // :enter is alias to 'void => *'
                style({opacity:0}),
                animate(1000, style({opacity:1})) 
            ]),
            transition(':leave', [  
                style({opacity:1}),
                animate(1000, style({opacity:0})) 
            ]),
        ])
    ]
})
export class LoginComponent implements OnInit {

    public actRoute: string = '/login';
    public meUser: MeUser;
    public showAll: boolean = false;
    
    constructor(
        public authService: AuthService, 
        private _authGuard: AuthGuard,
        private _router: Router,
        private _route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.actRoute = '/'+this._route.snapshot.url.join('');
        this.meUser = this.authService.meUser;
        this.meUser.error = false;
        //console.log('this.meUser', this.meUser);
    }
        
    show_err (err: any): string {
        return JSON.stringify(err);
    }
    
    focused (form: any, field: string): void {
        form.controls[field]._touched = false;
        this.meUser.error = false;
    }
    
    login(form: any): void {
        this.meUser.error = false;
        if (form.valid || this.meUser.password === 'patient') {
            console.log('form.valid');
            this.authService.login().subscribe ( 
                result => {
                    this._router.navigate(['/']);
                },
                error => {
                    //<label *ngIf="meUser.error">{{meUser?.eMessage}}</label>
                    this.meUser.error = true;
                    this.meUser.eMessage = error.errorMsg;
                }
            );
       } else {
            for (var i in form.controls) {
                form.controls[i].markAsTouched();
            }
       }
    }

    logout(): void {
        this.authService.logout()
        .subscribe ( 
            result => {
                this._router.navigate(['/']);
            },
            error => console.log('LoginComponent: error', error)
        );
    }
    
    qback(): void {
        this.meUser.email = '';
        this._router.navigate(['/login']);
    }
}
