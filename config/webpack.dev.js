const helper = require('./helpers');
const webpackMerge = require('webpack-merge'); // used to merge webpack configs
const commonConfig = require('./webpack.common.js'); // the settings that are common to prod and dev
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const port = 5001;

module.exports = webpackMerge(commonConfig, {
    devtool: 'cheap-module-source-map',

    output: {
        path: helper.root('dist'),
        publicPath: '/',//'http://localhost:' + port,
        filename: '[name].js',
        chunkFilename: '[id].chunk.js'
    },

//     plugins: [
//         new ExtractTextPlugin('[name].css', {allChunks: true})
//     ]

    devServer: {
        port: port,
        historyApiFallback: true,
        watchOptions: {
            ignored: [
                helper.root('node_modules'),
                /\.kate-swp$/
            ]
        }
    }
});
