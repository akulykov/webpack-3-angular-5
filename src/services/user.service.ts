import { Injectable } from '@angular/core';
import { Response } from '@angular/http';
import { HttpService } from './http.lib.service';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/throw';
import * as moment from 'moment'

import { User } from '~models/user.model';
 
@Injectable()
export class UserService {
    user: User = new User();
    constructor (
        private _http: HttpService
    ){};
    
    private _handleServerError(error: Response): Observable<Response> {
        if (error.json().success && error.json().result) {
            return Observable.throw(error.json().result);
        }
        return Observable.throw(error.json() || 'Server error');
    }

    private _post(url: string, data?: any): Observable<any> {
        let _data: string;
        if (!data) _data = '';
        else _data = JSON.stringify(data);
        return this._http.post(url, _data).map((res: Response) => {
            if (res.status === 200 && res.ok === true) {
                if (res.json().success === true && res.json().result) {
                    return res.json().result;
                }
            } 
            return res.json(); //fallback
        }).delay(0).catch( this._handleServerError );
    }

    loadUser(): Observable<any> {
        return this._post('patientInfo').map((res: any) => {
            this.user.info = res.patientInfo;
            this.user.info._init = true;
            return res;
        });
    }
    
//     loadChartData(): Observable<any> {
//         let data = {offset: 0, limit: 7};
//         return this._post('v1.0.1/medRecords/vitalSigns/all', data).map((res: any) => {
//             this.user.charts = res.vitalSignsData as UserChartsData;
//             this.user.charts._init = true;
//             return res;
//         });
//     }
    
//     sendIssue(postIssue: PostIssue): Observable<any> {
//         let data = {equipmentId: postIssue.deviceId, issue: postIssue.comment};
//         return this._post('equipment/issue', data).map((res: any) => {
//             return res;
//         });
//     }

}
