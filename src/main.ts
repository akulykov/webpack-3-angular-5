import { NgModule, enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { CookieModule, CookieService } from 'ngx-cookie';
import { FormsModule }   from '@angular/forms';

import { TranslateModule, TranslateLoader } from "@ngx-translate/core";
import { TranslateHttpLoader } from "@ngx-translate/http-loader";

import { Router }  from '@angular/router';
import { AppRoutingModule }   from '~modules/app-routing.module';
import { LoginRoutingModule } from '~modules/login-routing.module';

import { HttpService } from '~services/http.lib.service';
import { UserService } from '~services/user.service';
import { Helper }      from "~services/helper";

import { AppComponent } from '~components/app.component';
import { PageNotFoundComponent }  from '~components/not-found.component';
import { LoginComponent }  from '~components/login.component';
import { MainComponent }  from '~components/main.component';

if (process.env.ENV === 'production') enableProdMode();

export function HttpLoaderFactory(http: HttpClient) {
    let folder = '/src/i18n/';
    if (process.env.ENV === 'production') folder = '/assets/i18n/';
    return new TranslateHttpLoader(http, folder, ".json");
}

@NgModule({
  declarations: [
    MainComponent,
    AppComponent, LoginComponent,
    PageNotFoundComponent
  ],
  imports: [
    BrowserModule, HttpClientModule,
    FormsModule, BrowserAnimationsModule,
    LoginRoutingModule, AppRoutingModule,
    CookieModule.forRoot(),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    })
  ],
  providers: [
    {
      provide: HttpService,
      useFactory: (http: HttpClient, cookieService: CookieService) => {
        return new HttpService(http, cookieService);
      },
      deps: [HttpClient, CookieService]
    }, 
    UserService, Helper
  ],
  bootstrap: [AppComponent]
})

export class AppModule {    
    constructor(router: Router, helper: Helper) { 
//         //console.log('Routes: ', JSON.stringify(router.config, undefined, 2));
    }
}

platformBrowserDynamic().bootstrapModule(AppModule)
.catch(err => console.log(err));
