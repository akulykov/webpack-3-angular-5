import { HttpClient, HttpErrorResponse, HttpHeaders, HttpParams, HttpResponse  } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/materialize';
import 'rxjs/add/operator/dematerialize';

import { CookieService } from 'ngx-cookie';

export interface IRequestOptions {
  headers?: HttpHeaders;
  observe?: 'body';
  params?: HttpParams;
  reportProgress?: boolean;
  responseType?: 'json';
  withCredentials?: boolean;
  body?: any;
}

class MockRes {
    res: any;
    constructor (init: any) {
        this.res = init;
    }
    
    public json(): any {
        return {
            result: this.res
        }
    }
}

@Injectable()
export class HttpService {

  private _api: string = 'https://someurl.example';
  private _token: string|boolean = false;
  private _cookieTime: number = 1 * 3600 * 1000; ///60
  public lang: string = 'en';
    
  // Extending the HttpClient through the Angular DI.
  public constructor(public http: HttpClient, private _cookie: CookieService) {
    // If you don't want to use the extended versions in some cases you can access the public property and use the original one.
    // for ex. this.httpClient.http.get(...)
    
  }
  
  /**
    * GET request
    * @param {string} endPoint it doesn't need / in front of the end point
    * @param {IRequestOptions} options options of the request like headers, body, etc.
    * @returns {Observable<T>}
    */
    public get<T>(endPoint: string, options?: IRequestOptions): Observable<T> {
        return this.http.get<T>(this._api + endPoint, options);
    }

  /**
    * POST request
    * @param {string} endPoint end point of the api
    * @param {Object} params body of the request.
    * @param {IRequestOptions} options options of the request like headers, body, etc.
    * @returns {Observable<T>}
    */
    public post<T>(endPoint: string, params: Object, options?: IRequestOptions): Observable<T> {
        return this.http.post<T>(this._api + endPoint, params, options);
    }

  /**
    * PUT request
    * @param {string} endPoint end point of the api
    * @param {Object} params body of the request.
    * @param {IRequestOptions} options options of the request like headers, body, etc.
    * @returns {Observable<T>}
    */
    public put<T>(endPoint: string, params: Object, options?: IRequestOptions): Observable<T> {
        return this.http.put<T>(this._api + endPoint, params, options);
    }

  /**
    * DELETE request
    * @param {string} endPoint end point of the api
    * @param {IRequestOptions} options options of the request like headers, body, etc.
    * @returns {Observable<T>}
    */
    public delete<T>(endPoint: string, options?: IRequestOptions): Observable<T> {
        return this.http.delete<T>(this._api + endPoint, options);
    }
  
}
