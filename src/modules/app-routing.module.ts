import { NgModule }                from '@angular/core';
import { RouterModule, Routes }    from '@angular/router';

import { CanDeactivateGuard }            from '~services/can-deactivate-guard.service';
import { AuthGuard }                     from '~services/auth-guard.service';
import { SelectivePreloadingStrategy }   from './selective-preloading.strategy';

import { AppComponent }                  from '~components/app.component';
import { PageNotFoundComponent }         from '~components/not-found.component';

const appRoutes: Routes = [
    { path: '',
       component: AppComponent,
       canActivate: [AuthGuard],
       data: { title: '' }
    },
    { path: ':lang',
       component: AppComponent,
       canActivate: [AuthGuard],
       data: { title: '' }
    },
    { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes, { preloadingStrategy: SelectivePreloadingStrategy })
  ],
  exports: [
    RouterModule
  ],
  providers: [
    CanDeactivateGuard,
    SelectivePreloadingStrategy
  ]
})
export class AppRoutingModule { }
