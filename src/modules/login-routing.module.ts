import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard }            from '~services/auth-guard.service';
import { AuthService }          from '~services/auth.service';
import { UserService }          from '~services/user.service';
import { LoginComponent }  from '~components/login.component';
import { CookieService }       from 'ngx-cookie';

const loginRoutes: Routes = [
    { path: 'login', component: LoginComponent },
    { path: ':lang/login', component: LoginComponent }
];

@NgModule({
  imports: [
    RouterModule.forChild(loginRoutes)
  ],
  exports: [
    RouterModule
  ],
  providers: [
    AuthGuard,
    AuthService,
    CookieService,
    UserService
  ]
})
export class LoginRoutingModule {}
