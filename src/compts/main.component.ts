import 'rxjs/add/operator/switchMap';
import { Observable } from 'rxjs/Observable';
import { Component, OnInit, ViewChild } from '@angular/core';
import { style, state, animate, transition, trigger } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

import { UserService }  from '~services/user.service';
import { AuthService }  from '~services/auth.service';
import { MeUser }         from '~models/user.model';

@Component({
  selector: 'js-app',
  templateUrl: '../views/main.view.html'
})

export class MainComponent implements OnInit {

    public error: boolean = false;
    public validateErrors: any = {};
    public eMessage: string;

    
    constructor(
        private _userService: UserService,
        private _route: ActivatedRoute,
        private _router: Router
    ) {

    }

    ngOnInit() {

    }

//     @ViewChild('approveModal') public approveModal:ModalDirective;
//     public showApproveModal(visit: any):void {
//         this.selectedVisit = visit;
//         this.rejectVisit.visitId = visit.id;
//         this.error = false;
//         this.validateErrors = {};
//         this.approveModal.show();
//     }

}
