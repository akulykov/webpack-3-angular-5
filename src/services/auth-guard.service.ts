import { Injectable }       from '@angular/core';
import {
  CanActivate, Router, ActivatedRoute, Params,
  ActivatedRouteSnapshot, RouterStateSnapshot,
  CanActivateChild, NavigationExtras,
  CanLoad, Route
}                                       from '@angular/router';
import { AuthService }      from './auth.service';

@Injectable()
export class AuthGuard implements CanActivate, CanActivateChild, CanLoad {
    constructor (
        private _authService: AuthService, 
        private _router: Router,
        private _route: ActivatedRoute
    ){}

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
        this._authService.setLang(state.url.split('/')[1]);
        let url: string = this._authService.splitUrl(state.url);
        return this.checkLogin(url);
    }

    canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
        return this.canActivate(route, state);
    }

    canLoad(route: Route): boolean {
        let url = `/${route.path}`;
        return this.checkLogin(url);
    }

    checkLogin(url?: string): boolean {
        if (this._authService.isLoggedIn()) {
            return true;
        }
        this._router.navigate(['/login']);
        return false
    }

}
