import * as moment from 'moment';

export interface MeUser {
    token: string|boolean;
    email: string;
    password: string;
    fullName: string;
    error: any;
    eMessage: string;
    lang: string;
}

export class UserData {
    _init: boolean = false;
    _empty?: boolean = true;
    val?: Array<any>;
}

class UserMood {
    _init: boolean = false;
    _sent: boolean = false;
    val: string|boolean;
}


export class User {
    info: UserData = new UserData();
    mood: UserMood = new UserMood();

    fullName: string;
    constructor() {}
}
