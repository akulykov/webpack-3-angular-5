const helper = require('./helpers');

const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
// const rtl = require('postcss-rtl');

module.exports = {
  context: helper.root(),
  entry: {
      'vendor': [ helper.root('src', 'polyfills.ts'), helper.root('src', 'vendor.ts') ],
      'app': helper.root('src', 'main.ts')
  },
  resolve: {
      extensions: ['.ts', '.js'],
      alias: {
        "~components": helper.root('src', 'compts'),
        "~services":   helper.root('src', 'services'),
        "~modules":    helper.root('src', 'modules'),
        "~models":     helper.root('src', 'models'),
        "~views":      helper.root('src', 'views'),
        "~images":     helper.root('public', 'images'),
        "~styles":     helper.root('src', 'styles')
      }
  },
  module:{
    rules:[ 
          {
            test: /\.ts$/,
            use: [
                {
                    loader: 'awesome-typescript-loader',
                    options: { configFileName:  helper.root('tsconfig.json') }
                } ,
                'angular2-template-loader'
            ]
          },
          {
            test: /\.html$/,
            use: [
                {
                  loader: 'html-loader'
                }
            ]
          },
          {
            test: /\.css$/,
            include: helper.root('src', 'styles'),
            loader: 'raw-loader'
          }, 
          {
            test: /\.less$/,
            use: [{
                loader: "style-loader", // creates style nodes from JS strings
            }, {
                loader: "css-loader", // translates CSS into CommonJS
                options: {
                    sourceMap: true,
                    minimize: true,
                    importLoaders: 1
                }
            }, {
                loader: "postcss-loader", // creates style nodes from JS strings
                options: {
                  plugins: (loader) => [
//                     require('postcss-smart-import'),
                    require('autoprefixer')({
                        browsers: ['last 2 versions']
                    }),
                    require('postcss-rtl') ({
                      
                    })
                  ],
                  sourceMap: true
                }
            }, {
                loader: "less-loader", // compiles Less to CSS
                options: {
                    sourceMap: true
                }
            }]
          },
          {
            test: /\.(svg|woff|woff2|ttf|eot)$/,
            include: helper.root('src', 'styles', 'vendors'),
            loader: 'file-loader',
            options: {
              name: '[path][name].[ext]'
            }
          }, 
          {
            test: /\.svg$/,
            exclude: helper.root('src', 'styles', 'vendors', 'bootstrap'),
            use: [
              {
                loader: 'svg-sprite-loader',
                options: {}
              }
            ]
          }
    ]
  },
  plugins: [
    new webpack.ContextReplacementPlugin(
        /\@angular(\\|\/)core(\\|\/)esm5/,
        helper.root('src')
    ),

    new webpack.optimize.CommonsChunkPlugin({
        name: 'vendor',
        minChunks: Infinity,
    }),
    
    new webpack.optimize.CommonsChunkPlugin({
        name: "app",
        children: true,
        minChunks: 2
    }),
    
    new HtmlWebpackPlugin({
        filename: 'index.html',
        template: helper.root('src', 'views')+'/index.ejs',
        baseUrl: '/',
        chunks: ['vendor', 'app']
    })
  ]
}
