import { Injectable } from '@angular/core';
import { Response } from '@angular/http';
import { ActivatedRoute, Params } from '@angular/router';
import { HttpService } from './http.lib.service';
import { CookieService } from 'ngx-cookie';
import { TranslateService } from '@ngx-translate/core';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

import { MeUser } from '~models/user.model';

@Injectable()
export class AuthService {
    
    public meUser: MeUser;
    
    constructor (
        private _http: HttpService, 
        private _cookie: CookieService,
        private _route: ActivatedRoute,
        public translate: TranslateService
    ){
        this.translate.addLangs(["en", "ar"]);
        this.translate.setDefaultLang('en');
        this.meUser = {
            token: this._cookie.get('auth_token')||false,
            email: '',
            password: '',
            fullName: '',
            error: '',
            eMessage: '',
            lang: 'en'
        }
        //this.setLang();
    };
    
    public splitUrl (url: string): string 
    {
        let sUrl: Array<string> = url.split('/');
        if (sUrl[0] == '') sUrl.shift();
        if (sUrl[0] && sUrl[0].match(/en|ar|uk|us/)) sUrl.shift();
        return '/'+sUrl.join('/');
    }
    
    public setLang(lang?:string): void {
        if (lang && lang.match(/en|ar/)) { // found in url
            this.meUser.lang = lang;
        } else {
            if (this._cookie.get('lang') && this._cookie.get('lang').match(/en|ar/)) {
                this.meUser.lang = this._cookie.get('lang');
            } else {
                let browserLang: string;
                if (window.navigator.hasOwnProperty('userLanguage')) {
                    browserLang = window.navigator['userLanguage'];
                } else {
                    browserLang = window.navigator.language;
                }
                browserLang = browserLang.split('-')[0];
                if (browserLang && browserLang.match(/uk|us/)) browserLang = 'en';
                if (browserLang && browserLang.match(/en|ar/)) this.meUser.lang = browserLang;
            }
        }
        this._http.lang = this.meUser.lang;
        this._cookie.put('lang', this.meUser.lang);
        this.translate.use(this.meUser.lang);
        document.documentElement
        .setAttribute("dir", this.meUser.lang=='ar'?"rtl":'ltr');
        document.documentElement.setAttribute("lang", this.meUser.lang);
        //document.body.className = 'js-class-'+this.meUser.lang;
    }
    
    public isLoggedIn(): boolean {
        this.meUser.token= this._cookie.get('auth_token')||false;
        return this.meUser.token?true:false;
    }

    private handleServerError (error: Response): Observable<Response> {
        //console.log('handleServerError', error);
        if (error.status === 401) {
            //this.isLoggedIn = false;
        }
        if (error.json() && error.json().success && error.json().result) {
            return Observable.throw(error.json().result);
        }
        return Observable.throw(error.json() || 'Server error');
    }

    login(): Observable<any> {
        let data = JSON.stringify({"username":`${this.meUser.email}`, "password":`${this.meUser.password}`});
        
        this.meUser.token= false;
        this._cookie.remove('auth_token');
        return this._http.post('all/login', data).map((res: Response) => {
          if (res.status === 200 && res.ok === true) {
            if (res.json().success === true && res.json().result && res.json().result.token) {
                this.meUser.fullName = res.json().result.fullName;
                this.meUser.email = '';
                this.meUser.password = '';
                return res.json().result;
            }
          } 
          return res.json(); //fallback
        }).catch( this.handleServerError );
    }
    
    loadUser(): Observable<any> {
        return this._http.post('patientInfo', '{}').map((res: Response) => {
            return res.json();
        }).catch( this.handleServerError );
    }
    
    logout(): Observable<any> {
        return this._http.post('logout', '{}').map((res: Response) => {
            if (res.status === 200 && res.ok === true) {
                if (res.json().success === true) {
                    this.meUser.fullName = ''
                    this.meUser.email = '';
                    this.meUser.password = '';
                    this.meUser.token= false;
                    this._cookie.remove('auth_token');
                    return res.json().result;
                }
            } 
            return res.json(); //fallback
        }).catch( this.handleServerError );
    }
    


}
