import { Component } from '@angular/core';

import '~styles/main.less';

import '~images/svg/activity.svg';
import '~images/svg/calendar.svg';

import { Router, ActivatedRoute, Params } from '@angular/router';

import { AuthService }  from '~services/auth.service';
import { MeUser } from '~models/user.model';

@Component({
  selector: 'js-app',
  templateUrl: '../views/app.view.html'
})

export class AppComponent { 
    public user: MeUser;
    public actRoute: string = '/login';
    
    constructor(
        private _authService: AuthService,
        private _route: ActivatedRoute,
        private _router: Router
    ) {
        this.user = this._authService.meUser;
        console.log('this.user (meUser)', this.user);
        this.actRoute = this._authService.splitUrl(this._route.snapshot.url.join('/'));
        if (this.actRoute === '/') this.actRoute = '';
    }

}
